// vim:set ts=2 sw=2 et ai colorcolumn=90:
#ifndef _UTIL_H
#define _UTIL_H

#include <iostream>

namespace ctc {

/**
 * Outputs a line to STDERR with the given info, can be made conditional
 * by specifying cond. Should use macros, not this directly.
 */
inline void _log(const char* level,
                 int line,
                 const char* func,
                 const char* file,
                 const char* msg,
                 const char* msg2="",
                 bool cond=false) {
  if (!cond)
    std::cerr << "** " << level << ":" << file << "(L" << line << ")," << func
              << ": " << msg << msg2 << std::endl;
}

/**
 * If the given condition is false, outputs a failure message to 
 * STDERR and kills the program, exiting with status 1.
 *
 * Use macros rather than this directly.
 */
inline void _check_or_die(bool cond,
                          int line,
                          const char* func,
                          const char* file,
                          const char* msg,
                          const char* msg2="") {
  if (!cond) {
    _log("CHECK FAILED", line, func, file, msg, msg2);
    std::exit(1);
  }
}

#define CHECK(cond) _check_or_die((cond), __LINE__, __FUNCTION__, __FILE__, #cond)
#define CHECK_MSG(cond, msg) _check_or_die((cond), __LINE__, __FUNCTION__, __FILE__, \
                "cond:(" #cond "), msg: ", (msg))

#define WARN_C(cond) _log("WARN", __LINE__, __FUNCTION__, __FILE__, #cond, \
                          "", (cond))
#define WARN_CM(cond, msg) _log("WARN", __LINE__, __FUNCTION__, __FILE__, \
                                "cond:(" #cond "), msg: ", (msg), (cond))
#define WARN(msg) _log("WARN", __LINE__, __FUNCTION__, __FILE__, (msg))

} // namespace ctc

#endif  // _UTIL_H
