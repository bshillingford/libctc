// vim:set ts=2 sw=2 et ai colorcolumn=90:
#ifndef _CTC_IMPL_H
#define _CTC_IMPL_H

#include <cmath>
#include <iostream>
#include <limits>
#include <type_traits>
#include <Eigen/Dense>
#include <Eigen/Core>

#include "util.h"

namespace ctc {

using std::log1p;
using std::exp;

using Eigen::Array;
using Eigen::Matrix;
using Eigen::Map;
using Eigen::RowMajor;
using Eigen::Dynamic;

/**
 * Computes log(a+b) given log(a) and log(b).
 * Special cases are required to avoid subtracting infinities.
 */
template<typename T>
inline T log_add(const T& log_a, const T& log_b) {
  const static T LOG_ZERO = -std::numeric_limits<T>::infinity();
  if (log_a == LOG_ZERO) return log_b;
  if (log_b == LOG_ZERO) return log_a;
  return log_a + log1p(exp(log_b - log_a));
}

/**
 * Computes log(a+b) given log(a) and log(b), inplace.
 */
template<typename T>
inline T log_pluseq(T& log_a, const T& log_b) {
  const static T LOG_ZERO = -std::numeric_limits<T>::infinity();
  if (log_b == LOG_ZERO) return log_a;
  if (log_a == LOG_ZERO) return log_a = log_b;
  return log_a += log1p(exp(log_b - log_a));
}

/**
 * CTC loss implementation, templated over types. Holds reusable intermediate buffers, 
 * like log_alpha and log_beta.
 *
 * Error handling and checks are currently minimal, and can/should be extended to include
 * warnings for weird state. Ideally, compile it so it'll crash and exit at debug-time.
 *
 * References:
 * [1] Alex Graves' "preprint.pdf", chapter 7
 * [2] stanford-ctc: https://github.com/amaas/stanford-ctc/blob/master/ctc/ctc_fast.pyx
 *
 * Notes:
 * - real_t is either float, double, or something else C++ and Eigen supports.
 * - matrices are all last-dim-major, as in Torch (last stride = 1), and are 
 *   assumed to be contiguous.
 * - log_alpha and log_beta both use natural log arithmetic
 */
template<typename real_t, typename label_t>
struct CTC {
  static_assert(std::is_floating_point<real_t>::value, "real_t must be float type");
  static_assert(std::is_integral<label_t>::value,      "label_t must be integral");

  // Useful constants for log arithmetic
  constexpr static real_t LOG_ZERO = -std::numeric_limits<real_t>::infinity();
  constexpr static real_t LOG_ONE = 0;

  // Convenience typedefs, note the C-ordering
  typedef Eigen::Array<real_t, Dynamic, Dynamic, RowMajor> real_arr2;
  typedef Eigen::Matrix<real_t, Dynamic, 1> real_vec;
  typedef Eigen::Matrix<label_t, Dynamic, 1> label_vec;
  // note: mapped matrix would hence be Map<mat>, Map<mat, 0, ...stride... >, etc.

  real_arr2 log_alpha;  // T x U'
  real_arr2 log_beta;   // T x U'
  real_arr2 alphabeta;  // T x U'

  // stores result
  real_t loss;      // negative log probability of the target sequence
  real_t prob_seq;  // plain probability of the target sequence

  /**
   * Forward pass of CTC, returns loss value.
   *
   * Params:
   * logprobs   log probabilities from log-softmax, shape n_time x n_class
   * labels     target sequence (no blanks), shape n_label
   * n_time     T in notation of [1]
   * n_class    number of classes, INCLUDING the blank symbol, i.e. softmax size
   * n_label    length of target sequence, U in notation of [1]
   * blank_ind  ZERO-based index of "blank" symbol for the second dim of logprobs
   */
  real_t forward(const real_t* __restrict__ logprobs_,
                 const label_t* __restrict__ labels_,
                 size_t n_time,
                 size_t n_class,
                 size_t n_label,
                 label_t blank_ind) {
    // Notation from Graves' paper [1] and Eigen wrappers for arrays
    CHECK(n_label >= 1);       // for now, don't handle empty label seqs
    size_t T = n_time, U = n_label, Uprime = 2*n_label + 1;
    Map<const real_arr2> logprobs(logprobs_, n_time, n_class);
    Map<const label_vec> labels(labels_, n_label);

    // Check that n_time is sufficiently long for n_label:
    size_t time_needed = n_label;
    for (size_t u = 0; u < U - 1; u++)
      if (labels[u] == labels[u + 1])
        time_needed++;
    CHECK_MSG(n_time >= time_needed, "Not enough timesteps for label sequence");

    ////////////// Log of log_alpha /////////////
    // Fill up log_alpha.
    log_alpha.resize(T, Uprime);
    log_alpha.fill(LOG_ZERO);

    // Time t=0, init:
    log_alpha(0,0) = logprobs(0, blank_ind);
    log_alpha(0,1) = logprobs(0, labels[0]);

    // Time t>=1, recurse:
    for (size_t t = 1; t < T; t++) {
      // Loop within each col (over u'), see Fig 7.3 of [1]
      size_t min_u_p = (2*(T-t) >= Uprime) ? 0 : (Uprime - 2*(T-t));  // max(0, U'-2(T-t))
      size_t max_u_p = std::min(Uprime, 2*t + 2) - 1;
      // Note: my u' means [1]'s u in the equations:
      //   - u_prime is index into log_alpha, log_beta, blank-augmented label seq l', etc.
      //   - u is index into non-augmented label sequence l
      for (size_t u_prime = min_u_p; u_prime <= max_u_p; u_prime++) {
        // Separate cases for the Eqn 7.8 sum:
        if (u_prime % 2 == 0) {
          // Case 1: index u' is a blank (note: 0 indexed)
          real_t la;
          if (u_prime > 0) {  // sum except at first timestep, by Eqn 7.11
            la = log_add(log_alpha(t-1, u_prime), log_alpha(t-1, u_prime-1));
          } else {
            la = log_alpha(t-1, u_prime);
          }
          // Eqn 7.8: log(sum of prev alphas) + log(softmax prob)
          log_alpha(t, u_prime) = la + logprobs(t, blank_ind);
        } else {
          // Case 2: index u' is a label, at u=u'/2 in labels[u]
          size_t u = u_prime/2;
          // Always add current and previous u':
          real_t la = log_add(log_alpha(t-1, u_prime), log_alpha(t-1, u_prime-1));
          // Conditionally add u'-2 as in Eqn 7.9
          if (u_prime > 0 && (u >= 1 && labels(u) != labels(u-1))) {
            log_pluseq(la, log_alpha(t-1, u_prime-2));
          }
          // Eqn 7.8 again
          log_alpha(t, u_prime) = la + logprobs(t, labels(u));
        }
      }
    }

    // Use alphas to compute log probability of label seq:
    real_t log_prob_seq = log_add(log_alpha(T-1, Uprime-1), log_alpha(T-1, Uprime-2));
    loss = -log_prob_seq;
    prob_seq = exp(log_prob_seq);

    ////////////// Log of log_beta /////////////
    log_beta.resize(T, Uprime);
    log_beta.fill(LOG_ZERO);

    // Last timestep, init: Eqn 7.13
    log_beta(T-1, Uprime-1) = log_beta(T-1, Uprime-2) = LOG_ONE;

    // Iterate over time in reverse, T-2..0 inclusive, i.e. (T-2)-0+1 = T-1 iters
    // Note: counter needed since t >= 0 always true for unsigned t
    for (size_t counter=0, t = T-2; counter < T-1; counter++, t--) {
      // Loop over u' just as for log_alpha, see above:
      size_t min_u_p = (2*(T-t) >= Uprime) ? 0 : (Uprime - 2*(T-t));
      size_t max_u_p = std::min(Uprime, 2*t + 2) - 1;
      for (size_t u_prime = min_u_p; u_prime <= max_u_p; u_prime++) {
        // Separate cases for the Eqn 7.15 sum:
        if (u_prime % 2 == 0) {
          // Case 1: index u' is a blank
          // log of u' term in sum 7.15:
          real_t lb = log_beta(t+1, u_prime) + logprobs(t+1, blank_ind);
          // if not OOB, add second term:
          if (u_prime <= Uprime-2) {
            size_t u = u_prime/2;  // index into label seq: u = floor(u'/2)
            // log of u'+1 term
            log_pluseq(lb, log_beta(t+1, u_prime+1) + logprobs(t+1, labels[u]));
          }
          log_beta(t, u_prime) = lb;
        } else {
          // Case 2: index u' is a label, at u=u'/2 in labels[u]
          size_t u = u_prime/2;
          // Always add current and previous u':
          real_t lb = log_add(
              log_beta(t+1, u_prime) + logprobs(t+1, labels[u]),    // logged first term
              log_beta(t+1, u_prime+1) + logprobs(t+1, blank_ind)   // logged second term
          );
          // eqn 7.16: conditionally add log_beta u'+2, if l'_u' not in [blank, l'_{u'+2}]
          if (u_prime <= Uprime-3 && labels[u] != labels[u+1]) {
            log_pluseq(lb, log_beta(t+1, u_prime+2) + logprobs(t+1, labels[u+1]));
          }
          log_beta(t, u_prime) = lb;
        }
      }
    }

    // Store non-logged product of alpha*beta
    alphabeta = (log_alpha + log_beta).exp();

    // loss = negative log probability of the target
    return loss;
  }

  /**
  * Backward pass, stores result into dlogprobs, which has the same dims as logprobs.
  *
  * Assumes forward was already just called on exactly the same arguments. See forward for
  * parameter docs.
  */
  void backward(real_t* __restrict__ dlogprobs_,
                const real_t* __restrict__ logprobs_,
                const label_t* __restrict__ labels_,
                size_t n_time,
                size_t n_class,
                size_t n_label,
                label_t blank_ind) {
    CHECK(n_time >= n_label);  // approx, since AA needs to be output as A_A.
    CHECK(n_label >= 1);       // for now, don't handle empty label seqs
    size_t T = n_time, U = n_label, Uprime = 2*n_label + 1;
    Map<real_arr2> dlogprobs(dlogprobs_, n_time, n_class);
    Map<const real_arr2> logprobs(logprobs_, n_time, n_class);
    Map<const label_vec> labels(labels_, n_label);

    // Note, below, we're computing d(NLL for one example)/dlogy_k^t, unlike in 
    // ([1], eqn 7.31). Since, for a function f(y),
    //    df/dy = df/dlogy * dlogy/dy  =>  df/dlogy = df/dy * y,
    // we can rewrite Eqn 7.31 as, where y_k^t is the softmax output:
    //    d(NLL)/dlogy_k^t = y_k^t * d(NLL)/dlogy_k^t 
    //                     = -1/p(z|x) sum_u alpha(t,u) beta(t,u).
    // The alpha*beta terms have been precomputed and stored in this->alphabeta.

    // For each timestep t:
    dlogprobs.fill(0);
    for (size_t t = 0; t < T; t++) {
      // First, compute the sum, but instead of looping over B(z,k), sum indirectly:
      for (size_t u_prime = 0; u_prime < Uprime; u_prime++) {
        // Correct class for this position in the label sequence, if u' is
        // even, it's a blank; if odd, it's a label at index floor(u'/2)
        label_t lb = (u_prime % 2 == 0) ? blank_ind : labels(u_prime/2);
        dlogprobs(t, lb) += alphabeta(t, u_prime);  // alpha(t,u')*beta(t,u'), non-log
      }
    }
    // Multiply the whole dlogprobs array by -1/p(z|x), and we're done.
    dlogprobs /= -prob_seq;
  }
};

template<typename real_t, typename label_t>
constexpr real_t CTC<real_t, label_t>::LOG_ZERO;
template<typename real_t, typename label_t>
constexpr real_t CTC<real_t, label_t>::LOG_ONE;

/**
 * OpenMP wrappers for adding batch functionality
 */
namespace batch_wrapper {

/**
 * The batch version is similar to one-example version, except:
 *  - n_time becomes an array, the number of timesteps fed into LSTM for batch element i
 *  - similarly n_label[t] gives label sequence length for batch element i
 * and new batch strides are introduced:
 *  - logprobs_strides[i] denotes the number of elements of logprobs batch element i
 *  - labels_strides[i] denotes the number of elements of labels batch element i
 * Note: In practice former may often equal the constant value n_class*max_i'(n_time[i']) 
 *       for all i, and latter may often equal constant max_i(n_label[i]).
 * Note2: Values outside the elements jointly specified by n_time, n_class, 
 *        n_label, and the strides are guaranteed to be completely untouched.
 *
 * Return: per-batch-element CTC loss, summed, not divided by batch size.
 */
template<typename real_t, typename label_t>
real_t forward(
      std::vector<ctc::CTC<real_t, label_t> >& ctcs,
      size_t batch_size,
      const real_t* __restrict__ logprobs,
      const label_t* __restrict__ labels,
      const size_t* __restrict__ logprobs_strides,
      const size_t* __restrict__ labels_strides,
      const size_t* __restrict__ n_time,
      size_t n_class,
      const size_t* __restrict__ n_label,
      label_t blank_ind) {
  // allocate enough space in the vector (note: vector never shrinks)
  if (batch_size > ctcs.size())
    ctcs.resize(batch_size);  // resize calls constructors

  // run forward passes
  real_t loss_sum = 0;
  #pragma omp parallel for reduction(+ : loss_sum)
  for (size_t i = 0; i < batch_size; ++i) {
    real_t loss = ctcs[i].forward(
        logprobs + logprobs_strides[i]*i,
        labels + labels_strides[i]*i,
        n_time[i],
        n_class,
        n_label[i],
        blank_ind);
    loss_sum += loss;
  }
  return loss_sum;
}

/**
 * Changes are analogous to forward. dlogprobs is assumed to have identical striding as
 * logprobs.
 */
template<typename real_t, typename label_t>
void backward(
      std::vector<ctc::CTC<real_t, label_t> >& ctcs,
      size_t batch_size,
      real_t* __restrict__ dlogprobs,
      const real_t* __restrict__ logprobs,
      const label_t* __restrict__ labels,
      const size_t* __restrict__ logprobs_strides,
      const size_t* __restrict__ labels_strides,
      const size_t* __restrict__ n_time,
      size_t n_class,
      const size_t* __restrict__ n_label,
      label_t blank_ind) {
  #pragma omp parallel for
  for (size_t i = 0; i < batch_size; i++) {
    ctcs[i].backward(dlogprobs + logprobs_strides[i]*i,
                     logprobs + logprobs_strides[i]*i,
                     labels + labels_strides[i]*i,
                     n_time[i],
                     n_class,
                     n_label[i],
                     blank_ind);
  }
}

} // namespace ctc::batch_wrapper

} // namespace ctc

#endif // _CTC_IMPL_H
