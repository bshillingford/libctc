C, ffi = require 'ctc_ffi_gen'

n_class = 5
blank_ind = 0
n_label = 4
n_time = 9

torch.manualSeed(1)
logprobs = torch.rand(n_time, n_class)
logprobs:cdiv(logprobs:sum(2):expandAs(logprobs))  -- now sums to 1
logprobs:log()
labels = torch.IntStorage{3,4,2,1}

print(logprobs)
print(labels)

ctc = C.ctc_new_di()
print(tostring(C.ctc_typeinfo_di()))
loss = C.ctc_forward_di(ctc,
                        logprobs:storage():data(),
                        labels:data(),
                        n_time, n_class, n_label, blank_ind)

