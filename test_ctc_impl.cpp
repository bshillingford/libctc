// vim:set ts=2 sw=2 et ai colorcolumn=90:

#include <gtest/gtest.h>
#include <cmath>
#include <limits>
#include <iostream>
#include <random>
#include <Eigen/Dense>
#include <Eigen/Core>

#include "ctc_impl.h"

// Utilities:
#define DPR(X) std::cerr << #X << " =" << std::endl << (X) << std::endl;
using std::abs;
using std::exp;
using std::log;
using std::numeric_limits;

// Test fixture for data generation:
template <typename real_t>
class CtcTest : public testing::Test {
public:
  // Signed int32 to help catch index errors, and to match torch's IntTensor.
  typedef int32_t label_t;
  ctc::CTC<real_t, label_t> ctc;

  // Random data
  const static size_t n_time=5,   // timesteps of input
                      n_class=3,  // number of classes incl blank
                      n_label=3;  // length of label sequence
  const static label_t blank_ind=0;  // hence, 1..n_class-1 are non-blank
  // Use data() method to get pointers
  typedef Eigen::Matrix<real_t, n_time, n_class, Eigen::RowMajor> LogProbsType;
  LogProbsType logprobs;
  LogProbsType dlogprobs;
  Eigen::Matrix<label_t, n_label, 1> labels;

  // RNGs
  std::mt19937_64 rng;
  std::uniform_real_distribution<real_t> unif;
  std::uniform_int_distribution<label_t> randlabel;

  CtcTest() : randlabel(1, n_class-1) { }

  // Setup fake data: use ASSERTs so failure is fatal
  virtual void SetUp() {
    rng.seed(0);
    // Generate logprobs where each row is a prob dist
    logprobs = logprobs.unaryExpr([&](real_t x) { return unif(rng); });
    auto&& arr = logprobs.array();
    arr.colwise() /= arr.rowwise().sum();
    ASSERT_TRUE(((arr.rowwise().sum() - 1).abs() < 1e6).all());
    arr = arr.log();
    // Generate random label seq, excluding blanks i.e. [1, n_class-1]
    labels = labels.unaryExpr([&](label_t x) { return randlabel(rng); });
    // Start dlogprobs as inf so we can see if they're being overwritten
    dlogprobs.fill(std::numeric_limits<real_t>::infinity());
  }

  real_t call_forward_with_data() {
    return ctc.forward(logprobs.data(),
                       labels.data(),
                       n_time,
                       n_class,
                       n_label,
                       blank_ind);
  }

  void call_backward_with_data() {
    return ctc.backward(dlogprobs.data(),
                        logprobs.data(),
                        labels.data(),
                        n_time,
                        n_class,
                        n_label,
                        blank_ind);
  }
};
typedef testing::Types<float, double> RealTypes;
TYPED_TEST_CASE(CtcTest, RealTypes);

// Make sure log(0) == -inf as we expect, for floats and doubles
TEST(LogBehaviour, NegativeInf) {
  EXPECT_EQ(log(static_cast<float>(0)), -numeric_limits<float>::infinity());
  EXPECT_EQ(log(static_cast<double>(0)), -numeric_limits<double>::infinity());
  EXPECT_EQ(log(static_cast<long double>(0)), -numeric_limits<long double>::infinity());
}

// Instantiate CTC object without crashing
TYPED_TEST(CtcTest, Instantiate) { }

// Forward and backward work without crashing
TYPED_TEST(CtcTest, JustForwardAndBackward) {
  this->call_forward_with_data();
  this->call_backward_with_data();
}

// Automatically converts to float and double, giving a different threshold for each
struct error_threshold {
  operator float() { return 1e-5; }
  operator double() { return 1e-7; }
};

template<typename T1, typename T2>
inline bool small_rel_error(T1 cmp, T2 ref) {
  // make it symmetric:
  auto err1 = ((cmp - ref).abs() / abs(cmp)).maxCoeff();
  auto err2 = ((cmp - ref).abs() / abs(ref)).maxCoeff();
  return std::max(err1, err2) < static_cast<decltype(err1)>(error_threshold());
}

// Alpha and beta all sum to the same value for each column
TYPED_TEST(CtcTest, AlphaBetaAndLosses) {
  this->call_forward_with_data();
  const auto& ab = this->ctc.alphabeta;
  const auto& sums_per_timestep = ab.array().rowwise().sum();
  EXPECT_TRUE(small_rel_error(sums_per_timestep, sums_per_timestep[0]));

  // should also equal to the log prob, and to the loss
  EXPECT_NEAR(sums_per_timestep[0], this->ctc.prob_seq, error_threshold());
  EXPECT_NEAR(sums_per_timestep[0], exp(-this->ctc.loss), error_threshold());
}

// Finite-difference gradient check
TYPED_TEST(CtcTest, GradCheck) {
  // These are always double, for higher precision finite differences
  constexpr double eps    = 1e-4;
  constexpr double thresh = 1e-2;

  // Compute gradient and make a copy
  this->call_forward_with_data();
  this->call_backward_with_data();
  auto dlogprobs = this->dlogprobs;

  // Apply finite differences element-by-element; compare at each step:
  auto& logprobs = this->logprobs;
  for (size_t i = 0; i < logprobs.rows(); i++) {
    for (size_t j = 0; j < logprobs.cols(); j++) {
      // Compute, for dim i, [f(x+h)-f(x-h)]/(2eps)
      logprobs(i,j) += eps;
      this->call_forward_with_data();
      this->call_backward_with_data();
      //std::cout << this->ctc.loss << ' ';
      double finite_diff = this->ctc.loss;
      logprobs(i,j) -= 2*eps;
      this->call_forward_with_data();
      this->call_backward_with_data();
      //std::cout << this->ctc.loss << std::endl;
      logprobs(i,j) += eps;
      finite_diff -= this->ctc.loss;
      finite_diff /= 2.0*eps;
      // Check dloss/dlogprobs[i] approx equals finite diff
      EXPECT_NEAR(finite_diff, dlogprobs(i,j), thresh);
    }
  }
}

// Subclass for checking batch functionality consistency
// See torch code for more extensive gradient tests of batch version
template <typename real_t>
class CtcBatchTest : public CtcTest<real_t> {
public:
  real_t call_batch_forward() {
    // TODO: implement and write a test using it
    return 0;
  }
};
TYPED_TEST_CASE(CtcBatchTest, RealTypes);

