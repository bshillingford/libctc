#!/bin/bash

# Steps:
#  - remove includes like <cmath> which can't be resolved easily
#  - C preprocessor
#  - only keep our ctc_.* functions
#  - rename C++ pointer to the struct to a generic typedef'd void*
#  - adjust whitespace

OUTFILE="$1"

rm -fv ${OUTFILE}
{
cat <<'_EOF'
--[[
NOTE: this file is auto-generated as a build step.
Do not modify manually.
]]
local ffi = require 'ffi'
ffi.cdef[[
  typedef void ctc_handle_t;
  typedef void ctc_handle_b_t;

_EOF
cat exports.h \
  | grep -v "^#include" \
  | cpp \
  | sed -e 's/;/;\n/g' \
  | grep ctc_ \
  | sed -e 's/^ *//;
            s/ *$//;
            s/ _\?_\?restrict_\?_\? //g;
            s/std::vector<ctc::CTC<[^>]\+>[^>]\+>/ctc_handle_b_t/g;
            s/ctc::CTC<[^>]\+>/ctc_handle_t/g;' \
  | sort \
  | sed -e 's/^ */  /' \
  #| fmt -80 -s
  #| fold -w 75 -s --indent
cat <<'_EOF'
]]

-- Searches for and dlopen's libctc.so, ctc.so, ...
local ctc = ffi.load("ctc")
return ctc, ffi
_EOF
} > ${OUTFILE}

echo Generated ${OUTFILE}.
