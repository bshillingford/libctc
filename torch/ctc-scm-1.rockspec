package = "ctc"
version = "scm-1"

source = {
    url = "http://github.com/bshillingford/ctc",
    tag = "master"
}

description = {
    summary = "FFI bindings for a C++ CTC implementation",
    license = "BSD"
}

dependencies = {
    "torch >= 7.0",
    "nn"
}

build = {
    type = "command",
    build_command = [[
cmake -E make_directory build && cd build && cmake ../.. -Dbuild_torch=On -DLUALIB=$(LUALIB) -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH="$(LUA_BINDIR)/.." -DCMAKE_INSTALL_PREFIX="$(PREFIX)" && $(MAKE)
]],
    install_command = "cd build && $(MAKE) install"
}

