require 'totem'
require 'nn'
require 'ctc'

test = {}
local tester = totem.Tester()

------------------------- Start test helpers --------------------------
-- Random generation helpers
local function rand_logprobs(T, bs, classes)
  local tmp = torch.rand(T, bs, classes)
  return tmp:cdiv(tmp:sum(3):expandAs(tmp)):log()
end
local function rand_irange(shape, min, max)  -- inclusive
  return torch.rand(unpack(shape)):mul(max-min+1):add(min):floor():long()
end
-- Generate everything we need
local function generate(seed, bs)
  torch.manualSeed(seed or 1)  -- random tests are bad

  local t = {}
  -- dimensions/metadata:
  t.bs = bs
  t.blank_ind = 1
  t.n_class = torch.random(6, 10)
  -- NOTE: doesn't account for min timesteps due to duplicate consecutive outputs:
  t.labels_lengths = rand_irange({t.bs}, 2, 3)
  t.logprobs_lengths = t.labels_lengths:clone():mul(2):add(5) --"add" is arbitrary, must be >1
  -- the data itself:
  t.logprobs = rand_logprobs(t.logprobs_lengths:max(), bs, t.n_class)
  t.labels = rand_irange({bs, t.labels_lengths:max()}, 2, t.n_class)
  return t
end

-- Wrapper for criterions
local Mod, Mod_parent = torch.class('nn._ctc_criterion_wrap', 'nn.Module')
function Mod:__init(crit, target)
  Mod_parent.__init(self)
  self.crit = crit
  self.target = target
  self.output = torch.Tensor(1)
end
function Mod:updateOutput(input)
  self.output[1] = self.crit:updateOutput(input, self.target)
  return self.output
end
function Mod:updateGradInput(input, gradOutput)
  local critGO = self.crit:updateGradInput(input, self.target)
  self.gradInput:resizeAs(critGO):copy(critGO):mul(gradOutput[1])
  return self.gradInput
end

local function criterion_wrap(gen)
  local target = {gen.logprobs_lengths, gen.labels, gen.labels_lengths}
  local crit = ctc.CTCBatchCriterion(gen.blank_ind)
  local mod = nn._ctc_criterion_wrap(crit, target)
  return mod
end
------------------------- End test helpers --------------------------

function test.test_loss_averaging()
  local gen = generate(1, 1) -- seed, bs
  local mod = criterion_wrap(gen)
  local loss = mod:forward(gen.logprobs)

  -- Now, replicate the batch dimension and make sure the result is the same
  local rep = 5
  gen.labels_lengths = gen.labels_lengths:repeatTensor(rep)
  gen.logprobs_lengths = gen.logprobs_lengths:repeatTensor(rep)
  gen.logprobs = gen.logprobs:repeatTensor(1, rep, 1)
  gen.labels = gen.labels:repeatTensor(rep, 1)
  local mod_batched = criterion_wrap(gen)
  local loss_batched = mod_batched:forward(gen.logprobs)

  -- Should be equal if we're just summing
  tester:assertTensorEq(loss, loss_batched, 1e-10)
end

function test.gradcheck_batchsize1()
  local gen = generate(1, 1) -- seed, bs
  local mod = criterion_wrap(gen)
  totem.nn.checkGradients(tester, mod, gen.logprobs)
end

function test.gradcheck_batched()
  local gen = generate(1, 20) -- seed, bs
  local mod = criterion_wrap(gen)
  totem.nn.checkGradients(tester, mod, gen.logprobs)
end

return tester:add(test):run()
