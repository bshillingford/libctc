-- vim: ts=2 sw=2 et ai colorcolumn=100:

-- pregenerated ffi wrapper:
local ctcffi, ffi = require 'ctc_ffi_gen'

local ctcwrap = {}
ctcwrap.__index = ctcwrap

local float_type_map = {
  ['torch.DoubleTensor'] = 'd',
  ['torch.FloatTensor'] = 'f',
}
local label_type_map = {
  ['torch.IntTensor'] = 'i',
  ['torch.LongTensor'] = 'l',
}
function ctcwrap.new(float_type, label_type, batch)
  local self = setmetatable({}, ctcwrap)

  if batch == nil then batch = true end
  self.batch = batch

  -- Suffix for type overload
  if float_type_map[float_type] then
    float_type = float_type_map[float_type]
  end
  if label_type_map[label_type] then
    label_type = label_type_map[label_type]
  end
  assert(float_type == 'f' or float_type == 'd',
         'Must be f (32-bit) or d (64-bit) float type.')   
  assert(label_type == 'i' or label_type == 'l',
         'Must be i (32-bit) or l (64-bit) signed int.')   
  self.suffix = float_type .. label_type

  -- New instance of handle, see ffi declarations
  self.handle = self:F'new'()

  return self
end

-- Destructor: Lua 5.2 only: ignored for tables in some versions of Lua/LuaJIT
function ctcwrap:__gc()
  self:F'free'(self.handle)
end

-- Methods (take handle as first arg):
function ctcwrap:forward(...)
  return self:F'forward'(self.handle, ...)
end
function ctcwrap:backward(...)
  return self:F'backward'(self.handle, ...)
end

-- Functions:
function ctcwrap:typeinfo()
  -- Always non-batch naming pattern:
  return self:F('typeinfo', false)()
end

-- Produce function name and return FFI function:
function ctcwrap:F(name, batch)
  local batch = (batch==nil) and self.batch or batch

  func = "ctc_"
  if batch then
    func = func .. "batch_"
  end
  return ctcffi[func .. name .. "_" .. self.suffix]
end

return ctcwrap
