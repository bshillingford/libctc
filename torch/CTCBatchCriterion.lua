-- vim: ts=2 sw=2 et ai colorcolumn=100:
--[[
CTC criterion that takes a sequence of shape T x bs x (classes+1) as input.
Cannot be a table of tensors.

Target: a table {input_lengths, labels, label_lengths}, where:
  - input_lengths: 1D batch-sized contiguous LongTensor, number of input timesteps per batch elem.
  - labels: bs x T_labels, and contiguous. Type cannot change between calls to forward.
  - labels_lengths: 1D batch-sized contiguous LongTensor, length of target seq per batch element.

Note: this doesn't use the usual :type() method like other modules/criterions.
]]

local ctcwrap = require 'ctcwrap'
local ffi = require 'ffi'
local math = require 'math'

local CTCBatchCriterion, parent = torch.class('ctc.CTCBatchCriterion', 'nn.Criterion')

function CTCBatchCriterion:__init(blank_ind, size_average)
  parent.__init(self)

  if size_average ~= nil then
    self.size_average = size_average
  else
    self.size_average = true
  end
  self.blank_ind = blank_ind
  self.logprobs_buf = torch.Tensor()
  self.dlogprobs_buf = torch.Tensor()
  self.labels_buf = torch.LongTensor()
  -- int64_t: (we'll cast these to size_t (uint64_t) also, make sure they're the same size)
  assert(ffi.sizeof('int64_t') == ffi.sizeof('size_t'))
  self.logprobs_strides = torch.LongTensor()
  self.labels_strides = torch.LongTensor()
end

function CTCBatchCriterion:setBlankInd(blank_ind)
  self.blank_ind = blank_ind
end

function CTCBatchCriterion:type(typename)
  return self  -- no-op
end

function CTCBatchCriterion:updateOutput(input, target)
  local logprobs = input
  local logprobs_lengths, labels, labels_lengths = unpack(target)
  local T       = logprobs:size(1)
  local bs      = logprobs:size(2)
  local n_class = logprobs:size(3) -- includes blank

  -- Type and basic dimension checks
  if not self._ctc then
    self._input_type = torch.typename(logprobs)
    self._target_type = torch.typename(labels)
    self._ctc = ctcwrap.new(torch.typename(input), torch.typename(labels), true)
  end
  assert(self._input_type == torch.typename(logprobs), "input type cannot change")
  assert(self._target_type == torch.typename(labels),  "target type cannot change")
  assert(labels:size(1) == bs, "labels wrongly sized")
  assert(logprobs_lengths:size(1) == bs and labels_lengths:size(1) == bs, "lengths wrong size")
  -- contiguous lengths and labels:
  assert(logprobs_lengths:isContiguous() and labels:isContiguous()
         and labels_lengths:isContiguous())  

  -- Sanity checks for more subtle dimensions matching
  assert(labels_lengths:max() < T, "number of timesteps in model is too short to handle "
                                .. "the length of the label sequence")
  assert(labels_lengths:max() <= labels:size(2), "label seq length larger than provided labels")
  assert(logprobs_lengths:max() <= T, "input length specifies more timesteps than provided")
  -- TODO: another check (or test): forall b=1..bs, timesteps <= 2*labels+1, or more precise w/ blanks

  -- Prepare logprobs and strides array (length array is given)
  -- (logprobs_buf: bs x T x n_class)
  local logprobs_t = logprobs:transpose(1,2)
  self.logprobs_buf:typeAs(logprobs_t):resizeAs(logprobs_t):copy(logprobs_t)
  self.logprobs_strides:resize(bs):fill(T * n_class)

  -- Prepare labels and its strides array
  -- Labels in ctc library are zero-indexed, but in Torch are 1-indexed:
  self.labels_buf:resizeAs(labels):typeAs(labels):copy(labels):add(-1) 
  self.labels_strides:resize(bs):fill(labels:stride(1))

  self.output = self._ctc:forward(
    bs,                            -- size_t batch_size
    self.logprobs_buf:data(),      -- const real_t* logprobs
    self.labels_buf:data(),        -- const label_t* labels
    self.logprobs_strides:data(),  -- const size_t* logprobs_strides
    self.labels_strides:data(),    -- const size_t* labels_strides
    logprobs_lengths:data(),       -- const size_t* n_time
    n_class,                       -- size_t n_class
    labels_lengths:data(),         -- const size_t* n_label
    self.blank_ind-1)              -- label_t blank_ind
  if self.size_average then
    self.output = self.output / bs
  end
  return self.output
end

function CTCBatchCriterion:updateGradInput(input, target)
  local logprobs = input
  local logprobs_lengths, labels, labels_lengths = unpack(target)
  local T       = logprobs:size(1)
  local bs      = logprobs:size(2)
  local n_class = logprobs:size(3) -- includes blank
  -- In backward pass, SKIP dimension checks. Assume the same input as :updateOutput().

  -- Prep dlogprobs, i.e. self.gradInput.
  self.dlogprobs_buf:typeAs(self.logprobs_buf):resizeAs(self.logprobs_buf):fill(0)

  self._ctc:backward(
    bs,                           -- size_t batch_size
    self.dlogprobs_buf:data(),    -- real_t* dlogprobs               <== result written here
    self.logprobs_buf:data(),     -- const real_t* logprobs
    self.labels_buf:data(),       -- const label_t* labels
    self.logprobs_strides:data(), -- const size_t* logprobs_strides  <== shared for dlogprobs
    self.labels_strides:data(),   -- const size_t* labels_strides
    logprobs_lengths:data(),      -- const size_t* n_time
    n_class,                      -- size_t n_class
    labels_lengths:data(),        -- const size_t* n_label
    self.blank_ind-1)             -- label_t blank_ind

  local transpose = self.dlogprobs_buf:transpose(1,2)
  self.gradInput:typeAs(transpose):resizeAs(transpose):copy(transpose)
  if self.size_average then
    self.gradInput:div(bs)
  end
  return self.gradInput
end
