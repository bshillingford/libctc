// vim:set ts=2 sw=2 et ai colorcolumn=90:
#ifndef _EXPORTS_H
#define _EXPORTS_H

#include "util.h"
#include "ctc_impl.h"

/**
 * These macros create C-compatible wrappers for the CTC struct, substituting in
 * template parameters and using type suffixes to denote the overloads.
 *
 * The first creates header declarations, and the second creates implementations.
 * Be sure to update both when changing anything.
 */
#define DECL_GENERIC_CTC(SUFF, REAL_T, LABEL_T)                                   \
    ctc::CTC<REAL_T, LABEL_T>* ctc_new_##SUFF();                                  \
    void ctc_free_##SUFF(ctc::CTC<REAL_T, LABEL_T>* ctc);                         \
    const char* ctc_typeinfo_##SUFF();                                            \
    REAL_T ctc_forward_##SUFF(                                                    \
        ctc::CTC<REAL_T, LABEL_T>* ctc,                                           \
        const REAL_T* logprobs,                                                   \
        const LABEL_T* labels,                                                    \
        size_t n_time,                                                            \
        size_t n_class,                                                           \
        size_t n_label,                                                           \
        LABEL_T blank_ind);                                                       \
    void ctc_backward_##SUFF(                                                     \
        ctc::CTC<REAL_T, LABEL_T>* ctc,                                           \
        REAL_T* dlogprobs,                                                        \
        const REAL_T* logprobs,                                                   \
        const LABEL_T* labels,                                                    \
        size_t n_time,                                                            \
        size_t n_class,                                                           \
        size_t n_label,                                                           \
        LABEL_T blank_ind);
#define IMPL_GENERIC_CTC(SUFF, REAL_T, LABEL_T)                                   \
    ctc::CTC<REAL_T, LABEL_T>* ctc_new_##SUFF() {                                 \
      return new ctc::CTC<REAL_T, LABEL_T>;                                       \
    }                                                                             \
    void ctc_free_##SUFF(ctc::CTC<REAL_T, LABEL_T>* ctc) {                        \
      delete ctc;                                                                 \
    }                                                                             \
    const char* ctc_typeinfo_##SUFF() {                                           \
      return "<CTC types: suffix = " #SUFF                                        \
                  ", real_t = " #REAL_T                                           \
                  ", label_t = " #LABEL_T ">";                                    \
    }                                                                             \
    REAL_T ctc_forward_##SUFF(                                                    \
            ctc::CTC<REAL_T, LABEL_T>* ctc,                                       \
            const REAL_T* logprobs,                                               \
            const LABEL_T* labels,                                                \
            size_t n_time,                                                        \
            size_t n_class,                                                       \
            size_t n_label,                                                       \
            LABEL_T blank_ind) {                                                  \
      return ctc->forward(logprobs,                                               \
                          labels,                                                 \
                          n_time,                                                 \
                          n_class,                                                \
                          n_label,                                                \
                          blank_ind);                                             \
    }                                                                             \
    void ctc_backward_##SUFF(                                                     \
            ctc::CTC<REAL_T, LABEL_T>* ctc,                                       \
            REAL_T* dlogprobs,                                                    \
            const REAL_T* logprobs,                                               \
            const LABEL_T* labels,                                                \
            size_t n_time,                                                        \
            size_t n_class,                                                       \
            size_t n_label,                                                       \
            LABEL_T blank_ind) {                                                  \
      ctc->backward(dlogprobs,                                                    \
                    logprobs,                                                     \
                    labels,                                                       \
                    n_time,                                                       \
                    n_class,                                                      \
                    n_label,                                                      \
                    blank_ind);                                                   \
    }

/**
 * Similar to the above macros, but for batch mode.
 *
 * TODO: ideally, memory allocation for state variables would happen
 * in batches for the CTC struct, and the ctor would accept slices.
 */
#define DECL_GENERIC_CTC_BATCH(SUFF, REAL_T, LABEL_T)                           \
    std::vector<ctc::CTC<REAL_T, LABEL_T> >* ctc_batch_new_##SUFF();            \
    void ctc_batch_free_##SUFF(std::vector<ctc::CTC<REAL_T, LABEL_T> >* ctcs);  \
    REAL_T ctc_batch_forward_##SUFF(                                            \
        std::vector<ctc::CTC<REAL_T, LABEL_T> >* ctcs,                          \
        size_t batch_size,                                                      \
        const REAL_T* __restrict__ logprobs,                                        \
        const LABEL_T* __restrict__ labels,                                         \
        const size_t* __restrict__ logprobs_strides, \
        const size_t* __restrict__ labels_strides, \
        const size_t* __restrict__ n_time,                                          \
        size_t n_class,                                                         \
        const size_t* __restrict__ n_label,                                         \
        LABEL_T blank_ind);                                                     \
    void ctc_batch_backward_##SUFF(                                             \
        std::vector<ctc::CTC<REAL_T, LABEL_T> >* ctcs,                          \
        size_t batch_size,                                                      \
        REAL_T* __restrict__ dlogprobs,                                             \
        const REAL_T* __restrict__ logprobs,                                        \
        const LABEL_T* __restrict__ labels,                                         \
        const size_t* __restrict__ logprobs_strides, \
        const size_t* __restrict__ labels_strides, \
        const size_t* __restrict__ n_time,                                          \
        size_t n_class,                                                         \
        const size_t* __restrict__ n_label,                                         \
        LABEL_T blank_ind);
#define IMPL_GENERIC_CTC_BATCH(SUFF, REAL_T, LABEL_T)                           \
    std::vector<ctc::CTC<REAL_T, LABEL_T> >* ctc_batch_new_##SUFF() {           \
      return new std::vector<ctc::CTC<REAL_T, LABEL_T> >;                       \
    }                                                                           \
    void ctc_batch_free_##SUFF(std::vector<ctc::CTC<REAL_T, LABEL_T> >* ctcs) { \
      delete ctcs;                                                              \
    }                                                                           \
    REAL_T ctc_batch_forward_##SUFF(                                            \
          std::vector<ctc::CTC<REAL_T, LABEL_T> >* ctcs,                        \
          size_t batch_size,                                                    \
          const REAL_T* __restrict__ logprobs,                                      \
          const LABEL_T* __restrict__ labels,                                       \
          const size_t* __restrict__ logprobs_strides,  \
          const size_t* __restrict__ labels_strides,  \
          const size_t* __restrict__ n_time,                                        \
          size_t n_class,                                                       \
          const size_t* __restrict__ n_label,                                       \
          LABEL_T blank_ind) {                                                  \
      return ctc::batch_wrapper::forward(                                       \
          *ctcs,                                                                \
          batch_size,                                                           \
          logprobs,                                                             \
          labels,                                                               \
          logprobs_strides, \
          labels_strides, \
          n_time,                                                               \
          n_class,                                                              \
          n_label,                                                              \
          blank_ind);                                                           \
    }                                                                           \
    void ctc_batch_backward_##SUFF(                                             \
          std::vector<ctc::CTC<REAL_T, LABEL_T> >* ctcs,                        \
          size_t batch_size,                                                    \
          REAL_T* __restrict__ dlogprobs,                                           \
          const REAL_T* __restrict__ logprobs,                                      \
          const LABEL_T* __restrict__ labels,                                       \
          const size_t* __restrict__ logprobs_strides,  \
          const size_t* __restrict__ labels_strides,  \
          const size_t* __restrict__ n_time,                                        \
          size_t n_class,                                                       \
          const size_t* __restrict__ n_label,                                       \
          LABEL_T blank_ind) {                                                  \
      ctc::batch_wrapper::backward(                                             \
          *ctcs,                                                                \
          batch_size,                                                           \
          dlogprobs, \
          logprobs,                                                             \
          labels,                                                               \
          logprobs_strides, \
          labels_strides, \
          n_time,                                                               \
          n_class,                                                              \
          n_label,                                                              \
          blank_ind);                                                           \
    }


extern "C" {

// Header pre-declarations:
// (suffix, real_t, label_t)
DECL_GENERIC_CTC(fi, float, int32_t)
DECL_GENERIC_CTC(di, double, int32_t)
DECL_GENERIC_CTC(fl, float, int64_t)
DECL_GENERIC_CTC(dl, double, int64_t)
// Batch versions:
DECL_GENERIC_CTC_BATCH(fi, float, int32_t)
DECL_GENERIC_CTC_BATCH(di, double, int32_t)
DECL_GENERIC_CTC_BATCH(fl, float, int64_t)
DECL_GENERIC_CTC_BATCH(dl, double, int64_t)

}  // extern "C"

#endif  // _EXPORTS_H
