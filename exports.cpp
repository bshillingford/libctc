// vim:set ts=2 sw=2 et ai colorcolumn=90:
#include "exports.h"

extern "C" {

// Implement the C wrappers for the CTC functions
// (suffix, real_t, label_t)
IMPL_GENERIC_CTC(fi, float, int32_t)
IMPL_GENERIC_CTC(di, double, int32_t)
IMPL_GENERIC_CTC(fl, float, int64_t)
IMPL_GENERIC_CTC(dl, double, int64_t)
// Batch versions:
IMPL_GENERIC_CTC_BATCH(fi, float, int32_t)
IMPL_GENERIC_CTC_BATCH(di, double, int32_t)
IMPL_GENERIC_CTC_BATCH(fl, float, int64_t)
IMPL_GENERIC_CTC_BATCH(dl, double, int64_t)

}  // extern "C"
